#!/bin/bash

echo Create mysql file

if [ -d "/mysql/mysql" ]; then
    rm -R /var/lib/mysql/*
    cp -r /mysql/* /var/lib/mysql/
    chown -R mysql:mysql /var/lib/mysql/
fi

service mysql start

if [[ ! -v MYSQL_LOCAL ]]; then
    MYSQL_LOCAL=%
fi
if [[ ! -v MYSQL_PASS ]]; then
    MYSQL_PASS=''
fi

if [[ -v MYSQL_USER ]]; then
    mysql -uroot -p123123 -e "DROP USER '$MYSQL_USER'@'$MYSQL_LOCAL';"
    mysql -uroot -p123123 -e "GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_USER'@'$MYSQL_LOCAL' IDENTIFIED BY '$MYSQL_PASS';"
fi





