#!/bin/bash

set -e

usermod -u 1000 www-data

for FILE in /bash/*.sh; do
    if [[ "$FILE" != "/bash/run.sh" ]]; then
        sed -i 's/\r$//' $FILE
    fi
  
done

/bash/create_mysql.sh

exec supervisord -n



